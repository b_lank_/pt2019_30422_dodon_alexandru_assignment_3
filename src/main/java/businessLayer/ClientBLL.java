package businessLayer;

import java.util.List;

import businessLayer.validators.ClientValidator;
import dataAccessLayer.ClientDao;
import model.Client;

/**
 * @author alex
 *
 */
public class ClientBLL 
{
	private ClientDao dao;
	private List<Client> clients;
	private ClientValidator validator;
	
	public ClientBLL() 
	{
		dao = new ClientDao();
		clients = dao.selectAll();
		validator = new ClientValidator();
	}
	
	public void insert(int id, String first, String last, String addr, String email) throws IllegalArgumentException
	{
		Client c = new Client(id,first,last,addr,email);
		validator.validate(c, clients);
		clients.add(c);
		dao.insertObject(c, false);
	}
	
	public void update(Client toUpdate, int id, String first, String last, String addr, String email) throws IllegalArgumentException
	{
		Client c = new Client(-1,first,last,addr,email);
		validator.validate(c, clients);
		c.setId(id);
		dao.editObject(toUpdate, c, false);
		clients.remove(toUpdate);
		clients.add(c);
	}
	
	public void delete(Client toDelete)
	{
		dao.deleteObject(toDelete);
		clients.remove(toDelete);
	}
	
	public Client findBy(int id)
	{
		for (Client c : clients)
		{
			if (c.getId() == id)
			{
				return c;
			}
		}
		
		return null;
	}
	
	public void resynch()
	{
		clients = dao.selectAll();
	}

	/**
	 * @return the dao
	 */
	public ClientDao getDao() {
		return dao;
	}

	/**
	 * @return the clients
	 */
	public List<Client> getClients() {
		return clients;
	}
}
