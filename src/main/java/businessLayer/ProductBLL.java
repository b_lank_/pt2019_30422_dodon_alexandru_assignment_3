package businessLayer;

import java.util.List;

import businessLayer.validators.ProductValidator;
import dataAccessLayer.ProductDao;
import model.OrderProduct;
import model.Product;

/**
 * @author alex
 *
 */
public class ProductBLL 
{
	private ProductDao dao;
	private List<Product> products;
	private ProductValidator validator;

	public ProductBLL()
	{
		dao = new ProductDao();
		products = dao.selectAll();
		validator = new ProductValidator();
	}
	
	public void insert(int id, String name, String description, String producer, int quantityInStock, double buyPrice) throws IllegalArgumentException
	{
		Product c = new Product(id, name, description, producer, quantityInStock, buyPrice);
		validator.validate(c, products);
		products.add(c);
		dao.insertObject(c, false);
	}
	
	public void update(Product toUpdate, int id, String name, String description, String producer, int quantityInStock, double buyPrice) throws IllegalArgumentException
	{
		Product c = new Product(-1, name, description, producer, quantityInStock, buyPrice);
		validator.validate(c, products);
		c.setId(id);
		dao.editObject(toUpdate, c, false);
		products.remove(toUpdate);
		products.add(c);
	}
	
	public void delete(Product toDelete)
	{
		dao.deleteObject(toDelete);
		products.remove(toDelete);
	}
	
	public Product findBy(int id)
	{
		for (Product c : products)
		{
			if (c.getId() == id)
			{
				return c;
			}
		}
		
		return null;
	}
	
	public void resynch()
	{
		products = dao.selectAll();
	}
	/**
	 * @return the dao
	 */
	public ProductDao getDao() 
	{
		return dao;
	}

	/**
	 * @return the products
	 */
	public List<Product> getProducts() 
	{
		return products;
	}
}
