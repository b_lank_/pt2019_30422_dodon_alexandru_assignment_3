package businessLayer.validators;

import java.util.List;
import java.util.regex.Pattern;

import businessLayer.UtilityFoo;
import model.Client;

public class ClientValidator implements Validator<Client> 
{
	private static final String namePattern = "[^a-zA-Z]";
	/**
	 * @see RFC 5322
	 * @see emailregex.com
	 */
	private static final String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	@Override
	public void validate(Client t, List<Client> l) throws IllegalArgumentException
	{
		if (!UtilityFoo.idIsUnique(t.getId(), l))
		{
			throw new IllegalArgumentException("Id collision!");
		}
		
		if (t.getFirstName().matches(namePattern) || t.getLastName().matches(namePattern))	
		{
			throw new IllegalArgumentException("Non-letter characters in name!");
		}
		
		if (!t.getEmail().matches(emailPattern))
		{
			throw new IllegalArgumentException("Email not valid!");
		}
	}

}
