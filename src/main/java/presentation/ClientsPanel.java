package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import businessLayer.UtilityFoo;

@SuppressWarnings("serial")
public class ClientsPanel extends JPanel 
{
	private Controller controller;
	
	private JButton insert = new JButton("Insert");
	private JButton update = new JButton("Update");
	private JButton delete = new JButton("Delete");
	private JTable clientsT;
	private JScrollPane sc;

	private JLabel idl = new JLabel("Id");
	private JTextField idt = new JTextField();
	private JLabel  firstl = new JLabel("First name");
	private JTextField  firstt = new JTextField();
	private JLabel  lastl = new JLabel("Last name");
	private JTextField  lastt = new JTextField();
	private JLabel  addressl = new JLabel("Address");
	private JTextField  addresst = new JTextField();
	private JLabel  emaill = new JLabel("Email");
	private JTextField  emailt = new JTextField();
	private JLabel info = new JLabel("Information: ");
	private JLabel message = new JLabel("Messages will be displayed here.");
	
	public ClientsPanel(Controller co)
	{
		this.setLayout(new GridBagLayout());
		
		this.controller = co;
		
		this.clientsT = controller.getClientT1();
		this.sc = new JScrollPane(clientsT);
		
		clientsT.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		        if (clientsT.getSelectedRow() > -1) {
		        	idt.setText(clientsT.getValueAt(clientsT.getSelectedRow(), 0).toString());
		        	firstt.setText(clientsT.getValueAt(clientsT.getSelectedRow(), 1).toString());
		        	lastt.setText(clientsT.getValueAt(clientsT.getSelectedRow(), 2).toString());
		        	addresst.setText(clientsT.getValueAt(clientsT.getSelectedRow(), 3).toString());
		        	emailt.setText(clientsT.getValueAt(clientsT.getSelectedRow(), 4).toString());
		        }
		    }

		});
		
		insert.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.insertClient(Integer.parseInt(idt.getText()), firstt.getText(), lastt.getText(), 
							addresst.getText(), emailt.getText());
				}
				catch(IllegalArgumentException e)
				{
					message.setText(e.getMessage());
					e.printStackTrace();
				}
				controller.setModels();
			}	
		});	
		
		update.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					controller.updateClient(clientsT.getSelectedRow(), Integer.parseInt(idt.getText()), firstt.getText(), 
								lastt.getText(), addresst.getText(), emailt.getText());
				}
				catch(IllegalArgumentException ex)
				{
					message.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.resyncAll();
				controller.setModels();
			}
		});
		
		delete.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.deleteClient(clientsT.getSelectedRow());
				}
				catch(IllegalArgumentException ex)
				{
					message.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.resyncAll();
				controller.setModels();
			}
		});
		
		GridBagConstraints c;

		//buttons
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 10,5,10);
		this.add(this.insert, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.update, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.delete, c);
		
		//input
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.idl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.idt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.firstl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.firstt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.lastl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.lastt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 9;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.addressl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 10;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.addresst, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 11;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.emaill, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 12;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.emailt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 13;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.info, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 14;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.3;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 10,10);
		this.add(this.message, c);
		
		//table
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 100;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(10, 0, 10,10);
		this.add(sc, c);
		
		sc.setMinimumSize(clientsT.getMinimumSize());
	}
}
