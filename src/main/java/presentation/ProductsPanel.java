package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import businessLayer.UtilityFoo;

public class ProductsPanel extends JPanel 
{
	private Controller controller;
	
	private JButton insert = new JButton("Insert");
	private JButton update = new JButton("Update");
	private JButton delete = new JButton("Delete");

	private JLabel idl = new JLabel("Id");
	private JTextField idt = new JTextField();
	private JLabel  namel = new JLabel("Product name");
	private JTextField  namet = new JTextField();
	private JLabel  descl = new JLabel("Description");
	private JTextField  desct = new JTextField();
	private JLabel  producerl = new JLabel("Producer");
	private JTextField  producert = new JTextField();
	private JLabel  quantityl = new JLabel("Quantity in stock");
	private JTextField  quantityt = new JTextField();
	private JLabel  buyl = new JLabel("Buying price");
	private JTextField  buyt = new JTextField();
	private JLabel info = new JLabel("Information: ");
	private JLabel message = new JLabel("Messages will be displayed here.");
	private JTable productsT;
	private JScrollPane sc;

	public ProductsPanel(Controller co) 
	{
		this.setLayout(new GridBagLayout());
		
		this.controller = co;
		
		this.productsT = controller.getProductT1();
		this.sc = new JScrollPane(productsT);
		
		productsT.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		        if (productsT.getSelectedRow() > -1) {
		        	idt.setText(productsT.getValueAt(productsT.getSelectedRow(), 0).toString());
		        	namet.setText(productsT.getValueAt(productsT.getSelectedRow(), 1).toString());
		        	desct.setText(productsT.getValueAt(productsT.getSelectedRow(), 2).toString());
		        	producert.setText(productsT.getValueAt(productsT.getSelectedRow(), 3).toString());
		        	quantityt.setText(productsT.getValueAt(productsT.getSelectedRow(), 4).toString());
		        	buyt.setText(productsT.getValueAt(productsT.getSelectedRow(), 5).toString());
		        }
		    }

		});
		
		insert.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.insertProduct(Integer.parseInt(idt.getText()), namet.getText(), desct.getText(), 
							producert.getText(), Integer.parseInt(quantityt.getText()), Double.parseDouble(buyt.getText()));
				}
				catch(IllegalArgumentException e)
				{
					message.setText(e.getMessage());
					e.printStackTrace();
				}
				controller.setModels();
			}	
		});	
		
		update.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					controller.updateProduct(productsT.getSelectedRow(), Integer.parseInt(idt.getText()), namet.getText(), desct.getText(), 
							producert.getText(), Integer.parseInt(quantityt.getText()), Double.parseDouble(buyt.getText()));
				}
				catch(IllegalArgumentException ex)
				{
					message.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.resyncAll();
				controller.setModels();
			}
		});
		
		delete.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.deleteProduct(productsT.getSelectedRow());
				}
				catch(IllegalArgumentException ex)
				{
					message.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.resyncAll();
				controller.setModels();
			}
		});
		
		GridBagConstraints c;

		//buttons
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 10,5,10);
		this.add(this.insert, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.update, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 10,10);
		this.add(this.delete, c);
		
		//input
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.idl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.idt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.namel, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.namet, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.descl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.desct, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 9;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.producerl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 10;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.producert, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 11;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.quantityl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 12;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.quantityt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 13;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.buyl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 14;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.buyt, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 15;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.info, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 16;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.3;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 10,10);
		this.add(this.message, c);
		
		//table
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 100;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(10, 0, 10,10);
		this.add(sc, c);
		
		sc.setMinimumSize(sc.getMinimumSize());

	}
}
