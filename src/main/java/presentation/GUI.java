package presentation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import businessLayer.UtilityFoo;

/**
 * @author alex
 *
 */
public class GUI extends JFrame 
{
	private Controller controller;
	
	private ClientsPanel clients;
	private ProductsPanel products;
	private OrderPanel orders;
	private CreateOrderPanel create;
	private JTabbedPane tabbedPane;
	
	/**
	 * 
	 */
	private GUI()
	{
		super("Warehouse Database");
		this.setVisible(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.setLayout(new GridBagLayout());
		
		this.controller = new Controller();
		this.clients = new ClientsPanel(controller);
		this.products = new ProductsPanel(controller);
		this.orders = new OrderPanel(controller);
		this.create = new CreateOrderPanel(controller);
		
		this.tabbedPane = new JTabbedPane(JTabbedPane.LEFT, JTabbedPane.WRAP_TAB_LAYOUT);
		

		this.tabbedPane.addTab("Clients", null, clients, "Manage the clients");
		this.tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		this.tabbedPane.addTab("Products", null, products, "Manage the stock");
		this.tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
		this.tabbedPane.addTab("Orders", null, orders, "Manage the orders");
		this.tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
		this.tabbedPane.addTab("Create Order", null, create, "Create new orders");
		this.tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);		
		
		GridBagConstraints c;
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		this.add(this.tabbedPane, c);	
		
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setVisible(true);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//UtilityFoo.setupDevelopmentDB();
		
		SwingUtilities.invokeLater(
			new Runnable() 
			{
				public void run() 
				{
					@SuppressWarnings("unused")
					GUI app = new GUI();
				}
			}
		);
	}
}
