package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import businessLayer.UtilityFoo;
import businessLayer.validators.OrderProductValidator;
import model.OrderProduct;
import model.Product;

public class CreateOrderPanel extends JPanel 
{
	private Controller controller;
	private JTable clientT;
	private JTable productT;
	private JScrollPane sc1;
	private JScrollPane sc2;
	
	private JButton clear = new JButton("Clear all selections");
	private JLabel info = new JLabel("Information: ");
	private JLabel message = new JLabel("Messages will be displayed here.");
	private JButton addP = new JButton("Add product entry");
	private JButton remP = new JButton("Remove product entry");
	private JButton create = new JButton("Create order");
	private JLabel  quantityl = new JLabel("Quantity ordered");
	private JTextField  quantityt = new JTextField();
	private JLabel  pricel = new JLabel("Price each");
	private JTextField  pricet = new JTextField();
	
	private JTable workingT;
	private JScrollPane sc3;
	private List<OrderProduct> orderProducts = new ArrayList<OrderProduct>();
	private OrderProductValidator opVal;
	
	public CreateOrderPanel(Controller co) 
	{
		this.controller = co;
		this.clientT = controller.getClientT();
		this.productT = controller.getProductT();
		this.workingT = new JTable();
		this.sc1 = new JScrollPane(clientT);
		this.sc2 = new JScrollPane(productT);
		this.sc3 = new JScrollPane(workingT);
		this.opVal = new OrderProductValidator(controller.getOHBLL(), controller.getPBLL());
		
		productT.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
		{
		    @Override
		    public void valueChanged(ListSelectionEvent event) 
		    {
		        if (productT.getSelectedRow() > -1) 
		        {
		        	quantityt.setText(productT.getValueAt(productT.getSelectedRow(), 4).toString());
		        	pricet.setText(productT.getValueAt(productT.getSelectedRow(), 5).toString());
		        }
		    }
		});
		
		clear.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					orderProducts = new ArrayList<OrderProduct>();
				}
				catch(IllegalArgumentException e)
				{
					message.setText(e.getMessage());
					e.printStackTrace();
				}
				finally
				{
					workingT.setModel(UtilityFoo.createTable(orderProducts));
					controller.setModels();
				}
			}	
		});	
		
		addP.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{					
					if (Integer.parseInt(quantityt.getText()) > 
					controller.getProducts().get(productT.getSelectedRow()).getQuantityInStock())
					{
						throw new IllegalArgumentException("Not enough items in stock!");
					}
					
					if (productT.getSelectedRow() > -1)
					{
						orderProducts.add(new OrderProduct(0, 0, controller.getProducts().get(productT.getSelectedRow()).getId(), 
								Integer.parseInt(quantityt.getText()), Double.parseDouble(pricet.getText())));
					}
					else
					{
						message.setText("To add, please select a row.");
					}
				}
				catch(IllegalArgumentException e)
				{
					message.setText(e.getMessage());
					e.printStackTrace();
				}
				finally
				{
					workingT.setModel(UtilityFoo.createTable(orderProducts));
					controller.setModels();
				}
			}	
		});	
		
		remP.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					if (workingT.getSelectedRow() < -1)
					{
						throw new IllegalArgumentException("To remove, please select a product entry.");
					}
					orderProducts.remove(workingT.getSelectedRow());
				}
				catch(IllegalArgumentException e)
				{
					message.setText(e.getMessage());
					e.printStackTrace();
				}
				finally
				{
					controller.setModels();
					workingT.setModel(UtilityFoo.createTable(orderProducts));
					controller.setModels();
				}
			}	
		});	
		
		create.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					if (clientT.getSelectedRow() > -1)
					{					
						if (orderProducts.size() != 0)
						{				
							controller.CAinsertOrderHead(0, "Newly created order",
									Integer.parseInt(clientT.getValueAt(clientT.getSelectedRow(), 0).toString()));
							
							controller.resyncAll();
							
							int oid = controller.getOrderHeads().get(controller.getOrderHeads().size() - 1).getId();
							
							for (OrderProduct i : orderProducts)
							{
								controller.CAinsertOrderProduct(0, oid,
										i.getProductID(), i.getQuantityOrdered(), i.getPriceEach());
								
								int index = 0;
								for (Product j : controller.getProducts())
								{
									if (j.getId() == i.getProductID())
									{
										controller.updateProduct(index, j.getId(), j.getName(), 
												j.getDescription(), j.getProducer(), 
												j.getQuantityInStock() - i.getQuantityOrdered(), j.getBuyPrice());
										break;
									}
									index++;
								}
								
							}
		
							UtilityFoo.createBill(controller.getOrderHeads().get(controller.getOrderHeads().size() - 1), orderProducts, 
									controller.getProducts(), controller.getClients());
							
							message.setText("Order finalized and bill created!");
						}
						else
						{
							message.setText("Cannot create empty order.");
						}
					}
					else
					{
						message.setText("Need to select a client to create order.");
					}
				}
				catch(IllegalArgumentException e)
				{
					message.setText(e.getMessage());
					e.printStackTrace();
				}
				finally
				{
					controller.resyncAll();
					workingT.setModel(UtilityFoo.createTable(orderProducts));
					controller.setModels();
				}
			}	
		});	
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints c;
		
		//clients and products

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 10,0,10);
		this.add(new JLabel("Clients"), c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 100;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(10, 10,10,10);
		this.add(this.sc1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 10,0,10);
		this.add(new JLabel("Products"), c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 1;
		c.gridheight = 100;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(10, 10,10,10);
		this.add(this.sc2, c);
		
		//buttons and labels

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(10, 0,5,0);
		this.add(this.clear, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.info, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.message, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.addP, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.remP, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.create, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.quantityl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.quantityt, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.pricel, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 9;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 0,5,0);
		this.add(this.pricet, c);
		
		//workingT

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 10;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 1;
		c.insets = new Insets(5, 0,10,0);
		this.add(this.sc3, c);
	}
}
