package businessLayer;

import java.util.List;

import businessLayer.validators.OrderProductValidator;
import dataAccessLayer.OrderProductDao;
import model.OrderHead;
import model.OrderProduct;

/**
 * @author alex
 *
 */
public class OrderProductBLL
{
	private OrderProductDao dao;
	private List<OrderProduct> orderProducts;
	private OrderProductValidator validator;
	
	public OrderProductBLL(OrderHeadBLL oh, ProductBLL r)
	{
		dao = new OrderProductDao();
		orderProducts = dao.selectAll();
		validator = new OrderProductValidator(oh, r);
	}

	public void insert(int id, int orderID, int productID, int quantityOrdered, double priceEach) throws IllegalArgumentException
	{
		OrderProduct c = new OrderProduct(id, orderID, productID, quantityOrdered, priceEach);
		validator.validate(c, orderProducts);
		orderProducts.add(c);
		dao.insertObject(c, false);
	}
	
	public void CAinsert(int id, int orderID, int productID, int quantityOrdered, double priceEach) throws IllegalArgumentException
	{
		OrderProduct c = new OrderProduct(id, orderID, productID, quantityOrdered, priceEach);
		validator.validate(c, orderProducts);
		dao.insertObject(c, true);
	}
	
	public void update(OrderProduct toUpdate, int id, int orderID, int productID, int quantityOrdered, double priceEach) throws IllegalArgumentException
	{
		OrderProduct c = new OrderProduct(-1, orderID, productID, quantityOrdered, priceEach);
		validator.validate(c, orderProducts);
		c.setId(id);
		dao.editObject(toUpdate, c, false);
		orderProducts.remove(toUpdate);
		orderProducts.add(c);
	}
	
	public void delete(OrderProduct toDelete)
	{
		dao.deleteObject(toDelete);
		orderProducts.remove(toDelete);
	}
	
	public OrderProduct findBy(int id)
	{
		for (OrderProduct c : orderProducts)
		{
			if (c.getId() == id)
			{
				return c;
			}
		}
		
		return null;
	}

	public void resynch()
	{
		orderProducts = dao.selectAll();
	}
	/**
	 * @return the dao
	 */
	public OrderProductDao getDao() {
		return dao;
	}

	/**
	 * @return the orderProducts
	 */
	public List<OrderProduct> getOrderProducts() {
		return orderProducts;
	}
}
