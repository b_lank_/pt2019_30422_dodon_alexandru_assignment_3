/**
 * 
 */
package presentation;

import java.util.List;

import javax.swing.JTable;

import businessLayer.ClientBLL;
import businessLayer.OrderHeadBLL;
import businessLayer.OrderProductBLL;
import businessLayer.ProductBLL;
import businessLayer.UtilityFoo;
import model.Client;
import model.OrderHead;
import model.OrderProduct;
import model.Product;

/**
 * @author alex
 *
 */
public class Controller 
{
	private ClientBLL client;
	private OrderHeadBLL orderH;
	private OrderProductBLL orderP;
	private ProductBLL product;

	private JTable clientT;
	private JTable productT;
	private JTable clientT1;
	private JTable productT1;
	private JTable orderHT;
	private JTable orderPT;
	
	public Controller()
	{
		client = new ClientBLL();
		product = new ProductBLL();
		orderH = new OrderHeadBLL(client);
		orderP = new OrderProductBLL(orderH, product);

		clientT = new JTable();
		productT = new JTable();
		clientT1 = new JTable();
		productT1 = new JTable();
		orderHT = new JTable();
		orderPT = new JTable();
		
		this.setModels();
	}
	
	public void setModels()
	{
		clientT.setModel(UtilityFoo.createTable(client.getClients()));
		productT.setModel(UtilityFoo.createTable(product.getProducts()));
		clientT1.setModel(UtilityFoo.createTable(client.getClients()));
		productT1.setModel(UtilityFoo.createTable(product.getProducts()));
		orderHT.setModel(UtilityFoo.createTable(orderH.getOrderHeads()));
		orderPT.setModel(UtilityFoo.createTable(orderP.getOrderProducts()));
	}
	
	public void insertClient(int id, String first, String last, String addr, String email)
	{
		client.insert(id, first, last, addr, email);
	}
	
	public void updateClient(int updateID, int id, String first, String last, String addr, String email)
	{
		if (updateID == -1)
		{
			throw new IllegalArgumentException("To update, please select a row.");
		}
		client.update(client.getClients().get(updateID), id, first, last, addr, email);
	}
	
	public void deleteClient(int toDeleteId)
	{
		if (toDeleteId == -1)
		{
			throw new IllegalArgumentException("To delete, please select a row.");
		}
		client.delete(client.getClients().get(toDeleteId));
	}
	
	public void insertProduct(int id, String name, String description, String producer, int quantityInStock, double buyPrice)
	{
		product.insert(id, name, description, producer, quantityInStock, buyPrice);
	}
	
	public void updateProduct(int updateID, int id, String name, String description, String producer, int quantityInStock, double buyPrice)
	{
		if (updateID == -1)
		{
			throw new IllegalArgumentException("To update, please select a row.");
		}
		product.update(product.getProducts().get(updateID), id, name, description, producer, quantityInStock, buyPrice);
	}
	
	public void deleteProduct(int toDeleteId)
	{
		if (toDeleteId == -1)
		{
			throw new IllegalArgumentException("To delete, please select a row.");
		}
		product.delete(product.getProducts().get(toDeleteId));
	}
	
	public void updateOrderHead(int updateID, int id, String status, int clientID)
	{
		if (updateID == -1)
		{
			throw new IllegalArgumentException("To update, please select a row.");
		}
		orderH.update(orderH.getOrderHeads().get(updateID), id, status, clientID);
	}
	
	public void deleteOrderHead(int toDeleteId)
	{
		if (toDeleteId == -1)
		{
			throw new IllegalArgumentException("To delete, please select a row.");
		}
		orderH.delete(orderH.getOrderHeads().get(toDeleteId));
	}
	
	public void insertOrderProduct(int id, int orderID, int productID, int quantityOrdered, double priceEach)
	{
		orderP.insert(id, orderID, productID, quantityOrdered, priceEach);
	}
	
	public void updateOrderProduct(int updateID, int id, int orderID, int productID, int quantityOrdered, double priceEach)
	{
		if (updateID == -1)
		{
			throw new IllegalArgumentException("To update, please select a row.");
		}
		orderP.update(orderP.getOrderProducts().get(updateID), id, orderID, productID, quantityOrdered, priceEach);
	}
	
	public void deleteOrderProduct(int toDeleteId)
	{
		if (toDeleteId == -1)
		{
			throw new IllegalArgumentException("To delete, please select a row.");
		}
		orderP.delete(orderP.getOrderProducts().get(toDeleteId));
	}
	
	public void CAinsertOrderProduct(int id, int orderID, int productID, int quantityOrdered, double priceEach)
	{
		orderP.CAinsert(id, orderID, productID, quantityOrdered, priceEach);
	}
	
	public void CAinsertOrderHead(int id, String status, int clientID)
	{
		orderH.CAinsert(id, status, clientID);
	}
	
	public void resyncAll()
	{
		client.resynch();
		product.resynch();
		orderH.resynch();
		orderP.resynch();
	}
	
	public List<Client> getClients()
	{
		return client.getClients();
	}
	
	public List<Product> getProducts()
	{
		return product.getProducts();
	}
	
	public List<OrderHead> getOrderHeads()
	{
		return orderH.getOrderHeads();
	}

	public List<OrderProduct> getOrderProducts()
	{
		return orderP.getOrderProducts();
	}
	
	public OrderHeadBLL getOHBLL()
	{
		return orderH;
	}
	
	public ProductBLL getPBLL()
	{
		return product;
	}

	/**
	 * @return the clientT
	 */
	public JTable getClientT() 
	{
		return clientT;
	}

	/**
	 * @return the productT
	 */
	public JTable getProductT() 
	{
		return productT;
	}

	/**
	 * @return the clientT
	 */
	public JTable getClientT1() 
	{
		return clientT1;
	}

	/**
	 * @return the productT
	 */
	public JTable getProductT1() 
	{
		return productT1;
	}

	/**
	 * @return the orderHT
	 */
	public JTable getOrderHT() 
	{
		return orderHT;
	}

	/**
	 * @return the orderPT
	 */
	public JTable getOrderPT() 
	{
		return orderPT;
	}
}
