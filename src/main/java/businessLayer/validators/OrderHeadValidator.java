/**
 * 
 */
package businessLayer.validators;

import java.util.List;

import businessLayer.ClientBLL;
import businessLayer.UtilityFoo;
import model.Client;
import model.OrderHead;

/**
 * @author alex
 *
 */
public class OrderHeadValidator implements Validator<OrderHead> 
{
	private ClientBLL cb;
	
	public OrderHeadValidator(ClientBLL cb)
	{
		this.cb = cb;
	}
	
	@Override
	public void validate(OrderHead t, List<OrderHead> l) throws IllegalArgumentException
	{
		List<Client> u = cb.getClients();
	
		if (!UtilityFoo.idIsUnique(t.getId(), l))
		{
			throw new IllegalArgumentException("Id collision!");
		}
		
		if (UtilityFoo.idIsUnique(t.getClientID(), u))
		{
			throw new IllegalArgumentException("Client id is not valid!");
		}
	}
	
}
