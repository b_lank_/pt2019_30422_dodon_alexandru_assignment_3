package businessLayer;

import java.util.List;

import businessLayer.validators.OrderHeadValidator;
import dataAccessLayer.OrderHeadDao;
import model.Client;
import model.OrderHead;

/**
 * @author alex
 *
 */
public class OrderHeadBLL 
{
	private OrderHeadDao dao;
	private List<OrderHead> orderHeads;
	private OrderHeadValidator validator;
	
	public OrderHeadBLL(ClientBLL cb)
	{
		dao = new OrderHeadDao();
		orderHeads = dao.selectAll();
		validator = new OrderHeadValidator(cb);
	}
	
	public void insert(int id, String status, int clientID) throws IllegalArgumentException
	{
		OrderHead c = new OrderHead(id,status,clientID);
		validator.validate(c, orderHeads);
		orderHeads.add(c);
		dao.insertObject(c, false);
	}
	
	public void CAinsert(int id, String status, int clientID) throws IllegalArgumentException
	{
		OrderHead c = new OrderHead(id,status,clientID);
		validator.validate(c, orderHeads);
		dao.insertObject(c, true);
	}
	
	public void update(OrderHead toUpdate, int id, String status, int clientID) throws IllegalArgumentException
	{
		OrderHead c = new OrderHead(-1,status,clientID);
		validator.validate(c, orderHeads);
		c.setId(id);
		dao.editObject(toUpdate, c, false);
		orderHeads.remove(toUpdate);
		orderHeads.add(c);
	}
	
	public void delete(OrderHead toDelete)
	{
		dao.deleteObject(toDelete);
		orderHeads.remove(toDelete);
	}
	
	public OrderHead findBy(int id)
	{
		for (OrderHead c : orderHeads)
		{
			if (c.getId() == id)
			{
				return c;
			}
		}
		
		return null;
	}
	
	public void resynch()
	{
		orderHeads = dao.selectAll();
	}
	/**
	 * @return the dao
	 */
	public OrderHeadDao getDao() {
		return dao;
	}

	/**
	 * @return the orderHeads
	 */
	public List<OrderHead> getOrderHeads() {
		return orderHeads;
	}
}
