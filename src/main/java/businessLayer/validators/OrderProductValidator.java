/**
 * 
 */
package businessLayer.validators;

import java.util.List;

import businessLayer.OrderHeadBLL;
import businessLayer.ProductBLL;
import businessLayer.UtilityFoo;
import model.OrderHead;
import model.OrderProduct;
import model.Product;

/**
 * @author alex
 *
 */
public class OrderProductValidator implements Validator<OrderProduct> 
{
	private OrderHeadBLL oh;
	private ProductBLL p;
	
	public OrderProductValidator(OrderHeadBLL oh, ProductBLL p)
	{
		this.oh = oh;
		this.p = p;
	}

	@Override
	public void validate(OrderProduct t, List<OrderProduct> l) throws IllegalArgumentException
	{
		List<OrderHead> o = oh.getOrderHeads();
		List<Product> r = p.getProducts();
		
		if (!UtilityFoo.idIsUnique(t.getId(), l))
		{
			throw new IllegalArgumentException("Id collision!");
		}
		
		if (UtilityFoo.idIsUnique(t.getOrderID(), o))
		{
			throw new IllegalArgumentException("OrderHead id is not valid!");
		}
		
		if (UtilityFoo.idIsUnique(t.getProductID(), r))
		{
			throw new IllegalArgumentException("Product id is not valid!");
		}
		
		if (t.getQuantityOrdered() < 0)
		{
			throw new IllegalArgumentException("Negative quantity ordered!");
		}
		
		int inStock = 0;
		for (Product pr : p.getProducts())
		{
			if (pr.getId() == t.getProductID())
			{
				inStock = pr.getQuantityInStock();
			}
		}
		
		if (t.getQuantityOrdered() > inStock)
		{
			throw new IllegalArgumentException("Not enough items in stock!");
		}
		
		if (t.getPriceEach() < 0)
		{
			throw new IllegalArgumentException("Negative price!");
		}
	}

}
