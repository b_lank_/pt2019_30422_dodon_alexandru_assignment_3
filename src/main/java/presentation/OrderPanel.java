package presentation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import businessLayer.UtilityFoo;
import model.Product;

public class OrderPanel extends JPanel 
{

	private Controller controller;
	
	private JButton updateH = new JButton("Update header");
	private JButton deleteH = new JButton("Delete header");
	private JButton insertP = new JButton("Insert product");
	private JButton updateP = new JButton("Update product");
	private JButton deleteP = new JButton("Delete product");
	private JTable orderHT;
	private JTable orderPT;
	private JScrollPane sc1;
	private JScrollPane sc2;

	private JLabel id1l = new JLabel("Id");
	private JTextField id1t = new JTextField();
	private JLabel  statusl = new JLabel("Status");
	private JTextField  statust = new JTextField();
	private JLabel  cidl = new JLabel("Client id");
	private JTextField  cidt = new JTextField();
	private JLabel  id2l = new JLabel("Id");
	private JTextField  id2t= new JTextField();
	private JLabel  hidl= new JLabel("Order id");
	private JTextField  hidt = new JTextField();
	private JLabel  pidl = new JLabel("Product id");
	private JTextField  pidt = new JTextField();
	private JLabel  quantityl = new JLabel("Quantity ordered");
	private JTextField  quantityt = new JTextField();
	private JLabel  pricel = new JLabel("Price each");
	private JTextField  pricet = new JTextField();
	private JLabel info1 = new JLabel("Information: ");
	private JLabel message1 = new JLabel("Messages will be displayed here.");
	private JLabel info2 = new JLabel("Information: ");
	private JLabel message2 = new JLabel("Messages will be displayed here.");
	
	public OrderPanel(Controller co)
	{
		this.setLayout(new GridBagLayout());
		
		this.controller = co;
		
		this.orderHT = controller.getOrderHT();
		this.orderPT = controller.getOrderPT();
		this.sc1 = new JScrollPane(orderHT);
		this.sc2 = new JScrollPane(orderPT);

		orderHT.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
		{
		    @Override
		    public void valueChanged(ListSelectionEvent event) 
		    {
		        if (orderHT.getSelectedRow() > -1) 
		        {
		        	id1t.setText(orderHT.getValueAt(orderHT.getSelectedRow(), 0).toString());
		        	statust.setText(orderHT.getValueAt(orderHT.getSelectedRow(), 1).toString());
		        	cidt.setText(orderHT.getValueAt(orderHT.getSelectedRow(), 2).toString());
		        }
		    }
		});

		orderPT.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
		{
		    @Override
		    public void valueChanged(ListSelectionEvent event) 
		    {
		        if (orderPT.getSelectedRow() > -1) 
		        {
		        	id2t.setText(orderPT.getValueAt(orderPT.getSelectedRow(), 0).toString());
		        	hidt.setText(orderPT.getValueAt(orderPT.getSelectedRow(), 1).toString());
		        	pidt.setText(orderPT.getValueAt(orderPT.getSelectedRow(), 2).toString());
		        	quantityt.setText(orderPT.getValueAt(orderPT.getSelectedRow(), 3).toString());
		        	pricet.setText(orderPT.getValueAt(orderPT.getSelectedRow(), 4).toString());
		        }
		    }
		});
		
		updateH.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.updateOrderHead(orderHT.getSelectedRow(), Integer.parseInt(id1t.getText()), statust.getText(),
							Integer.parseInt(cidt.getText()));
				}
				catch(IllegalArgumentException e)
				{
					message1.setText(e.getMessage());
					e.printStackTrace();
				}
				controller.resyncAll();
				controller.setModels();
			}	
		});	
		
		deleteH.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					controller.deleteOrderHead(orderHT.getSelectedRow());
				}
				catch(IllegalArgumentException ex)
				{
					message1.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.resyncAll();
				controller.setModels();
			}
		});
		
		insertP.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.insertOrderProduct(Integer.parseInt(id2t.getText()), Integer.parseInt(hidt.getText()), 
							Integer.parseInt(pidt.getText()), Integer.parseInt(quantityt.getText()), 
							Double.parseDouble(pricet.getText()));
					
					int index = 0;
					for (Product i : controller.getProducts())
					{
						if (i.getId() == Integer.parseInt(pidt.getText()))
						{
							controller.updateProduct(index, i.getId(), i.getName(), 
									i.getDescription(), i.getProducer(), 
									i.getQuantityInStock() - Integer.parseInt(quantityt.getText()), i.getBuyPrice());
							break;
						}
						
						index++;
					}					
				}
				catch(IllegalArgumentException ex)
				{
					message2.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.setModels();
			}
		});
		
		updateP.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					if(orderPT.getSelectedRow() == -1)
					{
						throw new IllegalArgumentException("To update, please select a row.");
					}

					int	qinit = controller.getOrderProducts().get(orderPT.getSelectedRow()).getQuantityOrdered();

					controller.updateOrderProduct(orderPT.getSelectedRow(), Integer.parseInt(id2t.getText()), Integer.parseInt(hidt.getText()), 
							Integer.parseInt(pidt.getText()), Integer.parseInt(quantityt.getText()), 
							Double.parseDouble(pricet.getText()));
					
					int index = 0;
					for (Product i : controller.getProducts())
					{
						if (i.getId() == Integer.parseInt(pidt.getText()))
						{
							controller.updateProduct(index, i.getId(), i.getName(), 
									i.getDescription(), i.getProducer(), 
									i.getQuantityInStock() - Integer.parseInt(quantityt.getText()) + qinit,	i.getBuyPrice());
							break;
						}
						
						index++;
					}
				}
				catch(IllegalArgumentException ex)
				{
					message2.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.setModels();
			}
		});
		
		deleteP.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					controller.deleteOrderProduct(orderPT.getSelectedRow());
				}
				catch(IllegalArgumentException ex)
				{
					message2.setText(ex.getMessage());
					ex.printStackTrace();
				}
				controller.setModels();
			}
		});
		
		GridBagConstraints c;

		//buttons1
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 10,5,10);
		this.add(this.updateH, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.deleteH, c);

		//input1
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.id1l, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.id1t, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.statusl, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.statust, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.cidl, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.cidt, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 5,10);
		this.add(this.info1, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 9;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.3;
		c.weighty = 0;
		c.insets = new Insets(5, 10, 10,10);
		this.add(this.message1, c);
		
		//table1

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 0, 0,10);
		this.add(new JLabel("OrderHeads"), c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 1;
		c.gridheight = 100;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 1;
		c.insets = new Insets(10, 0, 10,10);
		this.add(sc1, c);
		
		//buttons2

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 10,5,10);
		this.add(this.insertP, c);	

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.updateP, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.deleteP, c);
		
		//input2

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.id2l, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.id2t, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.hidl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.hidt, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.pidl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.pidt, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 9;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.quantityl, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 10;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.quantityt, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 11;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.pricel, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 12;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.pricet, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 13;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5, 10,5,10);
		this.add(this.info2, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 14;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.3;
		c.weighty = 0;
		c.insets = new Insets(5, 10,10,10);
		this.add(this.message2, c);
		
		//table2

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 3;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10, 0, 0,10);
		this.add(new JLabel("OrderProducts"), c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 3;
		c.gridy = 1;
		c.gridheight = 100;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(10, 0, 10,10);
		this.add(sc2, c);
		
		sc1.setMinimumSize(orderHT.getMinimumSize());
		sc2.setMinimumSize(orderPT.getMinimumSize());
	}

}
