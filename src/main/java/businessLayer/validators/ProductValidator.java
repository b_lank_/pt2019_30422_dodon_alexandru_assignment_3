/**
 * 
 */
package businessLayer.validators;

import java.util.List;

import businessLayer.UtilityFoo;
import model.Product;

/**
 * @author alex
 *
 */
public class ProductValidator implements Validator<Product> 
{
	@Override
	public void validate(Product t, List<Product> l) throws IllegalArgumentException
	{
		if (!UtilityFoo.idIsUnique(t.getId(), l))
		{
			throw new IllegalArgumentException("Id collision!");
		}
		
		if (t.getQuantityInStock() < 0)
		{
			throw new IllegalArgumentException("Negative quantity in stock!");
		}
		
		if (t.getBuyPrice() < 0)
		{
			throw new IllegalArgumentException("Negative price!");
		}
	}

}
