package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * 
 * @author alex
 *
 * @param <T>
 */
public class AbstractDao<T> 
{
	/**
	 * 
	 */
	protected final Class<T> type;
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public AbstractDao() 
	{
		this.type = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	private String generateInsertString(boolean skipID)
	{
		StringBuilder s = new StringBuilder();
		s.append("INSERT INTO ");
		s.append(type.getSimpleName());
		s.append(" SET ");
		
		boolean primary = skipID;
		
		for (Field f : type.getDeclaredFields())
		{
			if (primary)
			{
				primary = false;
				continue;
			}
			
			s.append(f.getName());
			s.append(" = ?, ");
		}
		
		s.delete(s.length() - 2, s.length());
		s.append(";");
		
		return s.toString();
	}
	
	public void insertObject(T toInsert, boolean skipID)
	{
		Connection c = ConnectionFactory.getConnection();
		String s = generateInsertString(skipID);
		boolean prime = skipID;
		PreparedStatement insertEntry = null;
		int i = 0;
		
		try 
		{
			insertEntry = c.prepareStatement(s);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		for (Field f : type.getDeclaredFields())
		{
			if (prime) 
			{
				prime = false;
				continue;
			}
			
			try 
			{
				PropertyDescriptor pd = new PropertyDescriptor(f.getName(), type);
				Method method = pd.getReadMethod();
				insertEntry.setObject(++i, method.invoke(toInsert));
			} 
			catch (IntrospectionException e) 
			{
				e.printStackTrace();
			}
			catch (IllegalAccessException e) 
			{
				e.printStackTrace();
			}
			catch (IllegalArgumentException e) 
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		
		try
		{
			insertEntry.executeUpdate();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(c);
			ConnectionFactory.close(insertEntry);
		}
	}
	
	private String generateUpdateString(boolean skipID)
	{
		StringBuilder s = new StringBuilder();
		s.append("UPDATE ");
		s.append(type.getSimpleName());
		s.append(" SET ");
		
		boolean primary = skipID;
		
		for (Field f : type.getDeclaredFields())
		{
			if (primary)
			{
				primary = false;
				continue;
			}
			
			s.append(f.getName());
			s.append(" = ?, ");
		}
		
		s.delete(s.length() - 2, s.length());
		s.append(" WHERE ");
		s.append(type.getDeclaredFields()[0].getName());
		s.append(" = ?;");
		
		return s.toString();
	}
	
	public void editObject(T toEdit, T template, boolean skipID)
	{
		Connection c = ConnectionFactory.getConnection();
		String s = generateUpdateString(skipID);
		boolean prime = skipID;
		PreparedStatement updateEntry = null;
		PropertyDescriptor pd;
		int i = 0;
		
		try 
		{
			updateEntry = c.prepareStatement(s);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		for (Field f : type.getDeclaredFields())
		{
			if (prime) 
			{
				prime = false;
				continue;
			}
			
			try 
			{
				pd = new PropertyDescriptor(f.getName(), type);
				Method method = pd.getReadMethod();
				updateEntry.setObject(++i, method.invoke(template));
			} 
			catch (IntrospectionException e) 
			{
				e.printStackTrace();
			}
			catch (IllegalAccessException e) 
			{
				e.printStackTrace();
			}
			catch (IllegalArgumentException e) 
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		
		try 
		{
			pd = new PropertyDescriptor(type.getDeclaredFields()[0].getName(), type);
			Method method = pd.getReadMethod();
			updateEntry.setObject(++i, method.invoke(toEdit));
		}
		catch (SecurityException e1) 
		{
			e1.printStackTrace();
		}
		catch (IntrospectionException e1) 
		{
			e1.printStackTrace();
		} 
		catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		}
		catch (InvocationTargetException e) 
		{
			e.printStackTrace();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		try
		{
			updateEntry.executeUpdate();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(c);
			ConnectionFactory.close(updateEntry);
		}
	}
	
	public void deleteObject(T toDelete)
	{
		Connection c = ConnectionFactory.getConnection();
		PreparedStatement ps = null;
		
		try 
		{
			String identifier = type.getDeclaredFields()[0].getName();
			ps = c.prepareStatement("DELETE FROM " + type.getSimpleName() + " WHERE " + identifier + " = " + 
					(new PropertyDescriptor(identifier, type).getReadMethod().invoke(toDelete)));		
			ps.executeUpdate();
		}
		catch (SecurityException e) 
		{
			e.printStackTrace();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		catch (IntrospectionException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(c);
			ConnectionFactory.close(ps);
		}
	}
	
	protected List<T> selectObjectsBy(String condition)
	{
		Connection c = ConnectionFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try 
		{
			ps = c.prepareStatement("SELECT * FROM " + type.getSimpleName() + "  WHERE " + condition + ";");
			rs = ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(c);
			ConnectionFactory.close(ps);
		}
		
		return createObjects(rs);
	}
	
	public List<T> createObjects(ResultSet rs) 
	{
		List<T> l = new ArrayList<T>();
		
		try 
		{
			while (rs.next())
			{
				T instance = type.newInstance();
				
				for (Field f : type.getDeclaredFields())
				{
					Object value = rs.getObject(f.getName());
					PropertyDescriptor pd = new PropertyDescriptor(f.getName(), type);
					Method method = pd.getWriteMethod();
					method.invoke(instance, value);
				}

				l.add(instance);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		catch (InstantiationException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} 
		catch (IntrospectionException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (InvocationTargetException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(rs);
		}
		
		return l;
	}
	
	public List<T> selectAll()
	{
		Connection c = ConnectionFactory.getConnection();
		ResultSet rs = null;
		PreparedStatement selectAll = null;
		
		try 
		{
			selectAll = c.prepareStatement("SELECT * FROM " + this.type.getSimpleName() + ";");
			rs = selectAll.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(c);
			ConnectionFactory.close(selectAll);
		}
		
		return createObjects(rs);
	}
}