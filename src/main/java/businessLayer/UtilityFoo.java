package businessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.swing.table.DefaultTableModel;

import dataAccessLayer.ClientDao;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.OrderHeadDao;
import dataAccessLayer.OrderProductDao;
import dataAccessLayer.ProductDao;
import model.Client;
import model.OrderHead;
import model.OrderProduct;
import model.Product;

public class UtilityFoo 
{
	public static String getRandomString() 
	{
		StringBuilder res = new StringBuilder();
		Random rand = new Random();
		
		for (int i = 0, n = rand.nextInt(5) + 3; i < n; i++)
		{
			res.append((char)('a' + rand.nextInt(26)));
		}
		
		return res.toString();
	}

	public static boolean idIsUnique(int id, List<?> o)
	{
		for (Object i : o)
		{
			try 
			{
				Field f = i.getClass().getDeclaredField("id");
				PropertyDescriptor pd = new PropertyDescriptor(f.getName(), i.getClass());
				Method m = pd.getReadMethod();
				
				if (id == (int) m.invoke(i))
				{
					return false;
				}
			} 
			catch (NoSuchFieldException e) 
			{
				e.printStackTrace();
			} 
			catch (SecurityException e) 
			{
				e.printStackTrace();
			} 
			catch (IntrospectionException e) 
			{
				e.printStackTrace();
			} 
			catch (IllegalAccessException e)
			{
				e.printStackTrace();
			} 
			catch (IllegalArgumentException e) 
			{
				e.printStackTrace();
			} 
			catch (InvocationTargetException e) 
			{
				e.printStackTrace();
			}
		}
		
		return true;
	}

	public static void setupDevelopmentDB()
	{
		Connection c = ConnectionFactory.getConnection();
		Random rand = new Random();
		
		ClientDao client = new ClientDao();
		OrderHeadDao orderH = new OrderHeadDao();
		OrderProductDao orderP = new OrderProductDao();
		ProductDao product = new ProductDao();
		
		try 
		{
			c.prepareStatement("DELETE FROM Client").executeUpdate();
			c.prepareStatement("DELETE FROM OrderHead").executeUpdate();
			c.prepareStatement("DELETE FROM OrderProduct").executeUpdate();
			c.prepareStatement("DELETE FROM Product").executeUpdate();

			c.prepareStatement("ALTER TABLE Client AUTO_INCREMENT = 1").executeUpdate();
			c.prepareStatement("ALTER TABLE OrderHead AUTO_INCREMENT = 1").executeUpdate();
			c.prepareStatement("ALTER TABLE OrderProduct AUTO_INCREMENT = 1").executeUpdate();
			c.prepareStatement("ALTER TABLE Product AUTO_INCREMENT = 1").executeUpdate();
			
			for (int i = 0; i < 10; i++)
			{
				client.insertObject(new Client(), true);
				product.insertObject(new Product(), true);
			}
			
			for (int i = 0; i < 5; i++)
			{
				OrderHead o = new OrderHead();
				o.setClientID(rand.nextInt(10) + 1);
				orderH.insertObject(o, true);
				
				for (int j = 0, n = rand.nextInt(5) + 1; j <n; j++)
				{
					OrderProduct op = new OrderProduct();
					op.setOrderID(i + 1);
					op.setProductID(rand.nextInt(10) + 1);
					orderP.insertObject(op, true);
				}
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			ConnectionFactory.close(c);
		}
	}

	public static <T> DefaultTableModel createTable(List<T> objects)
	{
		Object[] header = new Object[objects.size() == 0 ? 0 : objects.get(0).getClass().getDeclaredFields().length];
		List<String[]> data = new ArrayList<String[]>();
		
		if (objects.size() != 0)
		{
			int i = 0;
			for (Field f : objects.get(0).getClass().getDeclaredFields())
			{
				header[i++] = f.getName();
			}
				
			for (Object o : objects)
			{
				List<String> row = new ArrayList<String>();
				
				for (Field f : o.getClass().getDeclaredFields())
				{
					try 
					{
						f.setAccessible(true);
						row.add(f.get(o).toString());
					}
					catch (IllegalArgumentException e) 
					{
						e.printStackTrace();
					}
					catch (IllegalAccessException e) 
					{
						e.printStackTrace();
					}
				}
				
				data.add(row.toArray(new String[0]));
			}
		}
		DefaultTableModel model = new DefaultTableModel(header, 0)
		{
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) 
	        {                
	                return false;               
	        };
	    };
		
		for (String[] j : data)
		{
			model.addRow(j);
		}
		
		return model;
	}

	private static Product search(int opID, List<Product> p)
	{
		for (Product i : p)
		{
			if (i.getId() == opID)
			{
				return i;
			}
		}
		
		return null;
	}
	
	public static void createBill(OrderHead oh, List<OrderProduct> op, List<Product> p, List<Client> c)
	{
		try
		{
			Client cl = null;
			
			for (Client i : c)
			{
				if (i.getId() == oh.getClientID())
				{
					cl = i;
					break;
				}
			}
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd|HH:mm:ss");
			Date date = new Date();
			String filename = "/home/alex/Documents/bill|"+ dateFormat.format(date) + "|orderID:" + oh.getId() + "|clientID:" + oh.getClientID() + ".txt";
			File file = new File(filename);
			if (!file.getParentFile().exists())
				file.getParentFile().mkdirs();
			if (!file.exists())
			    file.createNewFile();
			PrintWriter w = new PrintWriter(file);
			w.println("Bill for order with id " + oh.getId() + " and client id " + oh.getClientID());
			w.println("Date of creation: " + dateFormat.format(date));
			w.println("Client name:" + cl.getFirstName() + " " + cl.getLastName());
			w.println("Address: " + cl.getAddress());
			w.println("Status: " + oh.getStatus());
			w.println("\nItems:\n");
			
			double total = 0;
			
			for (OrderProduct i: op)
			{
				Product pr = search(i.getProductID(), p);
				
				if (p != null)
				{
					w.println("    Id|Name: " + pr.getId() + "|" + pr.getName());
					w.println("    Quantity: " + i.getQuantityOrdered());
					w.println("    PriceEach: " + i.getPriceEach() + "\n");
					total += i.getQuantityOrdered() * i.getPriceEach();
				}
			}
			
			w.println("Total cost: " + total);
			w.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
