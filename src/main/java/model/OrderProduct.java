package model;

import java.util.Random;

/**
 * 
 * @author alex
 *
 */
public class OrderProduct
{
	private int id;
	private int orderID;
	private int productID;
	private int quantityOrdered;
	private double priceEach;
	/**
	 * 
	 */
	public OrderProduct()
	{
		this.quantityOrdered = new Random().nextInt(20);
		this.priceEach = new Random().nextDouble() * 100 + 1;
	}
	/**
	 * 
	 */
	public OrderProduct(int id, int orderID, int productID, int quantityOrdered, double priceEach) 
	{
		this.id = id;
		this.orderID = orderID;
		this.productID = productID;
		this.quantityOrdered = quantityOrdered;
		this.priceEach = priceEach;
	}
	/**
	 * @return the id
	 */
	public int getId() 
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) 
	{
		this.id = id;
	}
	/**
	 * @return the orderID
	 */
	public int getOrderID() 
	{
		return orderID;
	}
	/**
	 * @param orderID the orderID to set
	 */
	public void setOrderID(int orderID) 
	{
		this.orderID = orderID;
	}
	/**
	 * @return the productID
	 */
	public int getProductID() 
	{
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(int productID) 
	{
		this.productID = productID;
	}
	/**
	 * @return the quantityOrdered
	 */
	public int getQuantityOrdered() 
	{
		return quantityOrdered;
	}
	/**
	 * @param quantityOrdered the quantityOrdered to set
	 */
	public void setQuantityOrdered(int quantityOrdered) 
	{
		this.quantityOrdered = quantityOrdered;
	}
	/**
	 * @return the priceEach
	 */
	public double getPriceEach() 
	{
		return priceEach;
	}
	/**
	 * @param priceEach the priceEach to set
	 */
	public void setPriceEach(double priceEach) 
	{
		this.priceEach = priceEach;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		return "OrderProduct [id=" + id + ", orderID=" + orderID + ", productID=" + productID + ", quantityOrdered="
				+ quantityOrdered + ", priceEach=" + priceEach + "]";
	}
}
