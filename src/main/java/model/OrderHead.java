package model;

import java.util.Random;

import businessLayer.UtilityFoo;

/**
 * 
 * @author alex
 *
 */
public class OrderHead
{
	private int id;
	private String status;
	private int clientID;
	/**
	 * 
	 */
	public OrderHead()
	{
		status = UtilityFoo.getRandomString();
	}
	/**
	 * 
	 */
	public OrderHead(int id, String status, int clientID)
	{
		this.id = id;
		this.status = status;
		this.clientID = clientID;
	}
	/**
	 * @return the id
	 */
	public int getId() 
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) 
	{
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public String getStatus() 
	{
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) 
	{
		this.status = status;
	}
	/**
	 * @return the clientID
	 */
	public int getClientID() 
	{
		return clientID;
	}
	/**
	 * @param clientID the clientID to set
	 */
	public void setClientID(int clientID) 
	{
		this.clientID = clientID;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		return "OrderHead [id=" + id + ", status=" + status + ", clientID=" + clientID + "]";
	}
}
