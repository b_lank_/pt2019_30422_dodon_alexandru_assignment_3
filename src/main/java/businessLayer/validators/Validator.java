/**
 * 
 */
package businessLayer.validators;

import java.util.List;

/**
 * @author alex
 *
 */
public interface Validator<T> 
{
	public void validate(T t, List<T> l) throws IllegalArgumentException;
}
