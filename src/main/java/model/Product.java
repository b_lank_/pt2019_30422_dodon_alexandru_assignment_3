package model;

import java.util.Random;

import businessLayer.UtilityFoo;

/**
 * 
 * @author alex
 *
 */
public class Product 
{
	private int id;
	private String name;
	private String description;
	private String producer;
	private int quantityInStock;
	private double buyPrice;
	/**
	 * 
	 */
	public Product()
	{
		name = UtilityFoo.getRandomString() + UtilityFoo.getRandomString() + UtilityFoo.getRandomString();
		description = UtilityFoo.getRandomString() + UtilityFoo.getRandomString() + UtilityFoo.getRandomString() 
			+ UtilityFoo.getRandomString() + UtilityFoo.getRandomString() + UtilityFoo.getRandomString();
		producer = UtilityFoo.getRandomString();
		quantityInStock = new Random().nextInt(50);
		buyPrice = new Random().nextDouble() * 100;
	}
	/**
	 * 
	 */	
	public Product(int id, String name, String description, String producer, int quantityInStock, double buyPrice)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.producer = producer;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
	}
	/**
	 * @return the id
	 */
	public int getId() 
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) 
	{
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() 
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) 
	{
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() 
	{
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) 
	{
		this.description = description;
	}
	/**
	 * @return the producer
	 */
	public String getProducer() 
	{
		return producer;
	}
	/**
	 * @param producer the producer to set
	 */
	public void setProducer(String producer) 
	{
		this.producer = producer;
	}
	/**
	 * @return the quantityinStock
	 */
	public int getQuantityInStock() 
	{
		return quantityInStock;
	}
	/**
	 * @param quantityinStock the quantityinStock to set
	 */
	public void setQuantityInStock(int quantityInStock) 
	{
		this.quantityInStock = quantityInStock;
	}
	/**
	 * @return the buyPrice
	 */
	public double getBuyPrice() 
	{
		return buyPrice;
	}
	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(double buyPrice) 
	{
		this.buyPrice = buyPrice;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", producer=" + producer
				+ ", quantityinStock=" + quantityInStock + ", buyPrice=" + buyPrice + "]";
	}
}
