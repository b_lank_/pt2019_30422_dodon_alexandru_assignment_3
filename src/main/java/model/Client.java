package model;

import java.util.Random;
import businessLayer.UtilityFoo;

/**
 * 
 * @author alex
 *
 */
public class Client 
{
	private int id;
	private String firstName;
	private String lastName;
	private String address;
	private String email;
	/**
	 * 
	 */
	public Client()
	{
		id = new Random().nextInt(100);
		firstName = UtilityFoo.getRandomString();
		lastName = UtilityFoo.getRandomString() + UtilityFoo.getRandomString();
		address = "Street " + UtilityFoo.getRandomString() + " no. " + new Random().nextInt(100) + ", City " + UtilityFoo.getRandomString();
		email = UtilityFoo.getRandomString() + "@" + (new Random().nextBoolean() ? "gmail" : "yahoo") + ".com";
	}
	/**
	 * 
	 */
	public Client(int id, String first, String last, String addr, String email)
	{
		this.id = id;
		this.firstName = first;
		this.lastName = last;
		this.address = addr;
		this.email = email;
	}
	/**
	 * @return the id
	 */
	public int getId() 
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) 
	{
		this.id = id;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() 
	{
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() 
	{
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() 
	{
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) 
	{
		this.address = address;
	}
	/**
	 * @return the email
	 */
	public String getEmail() 
	{
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) 
	{
		this.email = email;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		return "Client [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address
				+ ", email=" + email + "]";
	}
}
