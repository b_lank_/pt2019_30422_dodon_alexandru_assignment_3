package dataAccessLayer;

import java.sql.*;
import java.util.logging.*;

/**
 * This class implements a singleton pattern and defines the mariadb database connection configuration.
 * This class is a singleton as we need only create one factory object and many Connection objects.
 * 
 * @author alex
 */
public class ConnectionFactory 
{
	/**
	 * A string containing the mariadb database url in the form jdbc:subprotocol:subname
	 */
	public static final String URL = "jdbc:mariadb://localhost:3306/ptWarehouseDB";
	/**
	 * A string containing the user for accessing a mariadb database
	 */
	public static final String USER = "ptWarehouseDBadmin";
	/**
	 * A string containing the password of the user for accessing a mariadb database
	 */
	public static final String PASSWORD = "project3";
	/**
	 * 
	 */
	public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver"; 
	
	/**
	 * Unique instantiation of the ConnectionFactory
	 */
	private static ConnectionFactory singleton = new ConnectionFactory();
	
	/**
	 * Constructs a ConnectionFactory.
	 * This is a private constructor as the class will be instantiated only once.
	 */
	private ConnectionFactory() 
	{
		//Figure out why this
		//Class.forName(DRIVER_CLASS);
	}
	
	/**
	 * Attempts to establish a connection to a mariadb database using the provided user and password.
	 * 
	 * @return a connection to the database
	 */
	private Connection createConnection() 
	{
		Connection connection = null;
		
		try 
		{
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return connection;
	}	
	
	/**
	 * Attempts to pass a connection to the database.
	 * 
	 * @return a connection to the database
	 */
	public static Connection getConnection() 
	{
		return singleton.createConnection();
	}
	
	/**
	 * Releases a Statement object's database and JDBC resources immediately instead of waiting for this to happen when it is automatically closed.
	 * Handles a null reference as input.
	 */
	public static void close(Statement o)
	{
		if (o != null)
		{
			try 
			{
				o.close();
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Releases a Connection object's database and JDBC resources immediately instead of waiting for them to be automatically released.
	 * Handles a null reference as input.
	 */
	public static void close(Connection o)
	{
		if (o != null)
		{
			try 
			{
				o.close();
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Releases a ResultSet object's database and JDBC resources immediately instead of waiting for this to happen when it is automatically closed.
	 * Handles a null reference as input.
	 */
	public static void close(ResultSet o)
	{
		if (o != null)
		{
			try 
			{
				o.close();
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
	}
}